Angularank App

Hello,
I have a few informations before You run the Application.

1] 
Due to GitHub rate limit (60 per hour) I had to add a access_token to each request to API in the code (it increases the limit to 5000 per hour). Without this token rate limit will finish very quickly.
The token is stored in Common/Constants.js file and it has to be changed to Your Personal Token after generated it on GitHub.
The token in app source will be invalid because I had to change it due to security reasons.

2]
Also in Common/Constants.js You can change the REPOSLIMIT constant to Your own.
This constant contain number of repos should be requested while main user ranking is loaded.
I had to do this because when application try to fetch all contributors based on repos, the app musts generate a large number of request ~300 (300s/5min). So this hangs the app and loading data is very long.
I couldn't solve this problem so I decided to limit it at start for better fluentness and performace.