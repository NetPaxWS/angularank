import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import Home from './Components/Home';
import Contributors from './Components/Contributors';
import Contributor from './Components/Contributor';
import Repos from './Components/Repos';
import Repo from './Components/Repo';
import About from './Components/About';
import NotFound from './Components/NotFound';
import Header from './Components/Header';
import Navbar from './Components/Navbar';
import './App.css';

class App extends Component {
  setAppHeader(Component, PageType) {
    return (props) => (
      <div className="app__wrapper">
        <Header page={PageType} />
        <Component {...props} />
      </div>
    );
  }

  render() {
    return (
      <Router>
        <div className="app">
          <Switch>
            <Route exact path="/" component={this.setAppHeader(Home, 'main')} />
            <Route path="/repos/" component={this.setAppHeader(Repos, 'sub')} />
            <Route path="/contributors/" component={this.setAppHeader(Contributors, 'sub')} />
            <Route path="/contributor/:contributorLogin" component={this.setAppHeader(Contributor, 'sub')} />
            <Route path="/contributor/" component={this.setAppHeader(Contributor, 'sub')} />
            <Route path="/repo/:repoName" component={this.setAppHeader(Repo, 'sub')} />
            <Route path="/repo/" component={this.setAppHeader(Repo, 'sub')} />
            <Route path="/about/" component={this.setAppHeader(About, 'sub')} />
            <Route component={this.setAppHeader(NotFound, 'sub')} />
          </Switch>
          <Navbar />
        </div>
      </Router>
    );
  }
}

export default App;
