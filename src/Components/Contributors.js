import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as constants from '../Common/Constants';
import {
  getHeaders,
  getData
} from '../Common/Functions';
import Loader from './Loader';
import logo from '../Images/logo.svg';

class Contributors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contributorsHeadres: [
        {
          "header_key": "login",
          "header_title": "User"
        },
        {
          "header_key": "contributions",
          "header_title": "Contributions"
        },
        {
          "header_key": "followers",
          "header_title": "Followers"
        },
        {
          "header_key": "public_repos",
          "header_title": "Repos"
        },
        {
          "header_key": "public_gists",
          "header_title": "Gists"
        }
      ],
      contributorsData: [],
      isLoadingRanking: 'loading',
      isLoadingStats: 'loaded',
      sortby: null,
      descending: false,
      errorText: ''
    };
  }

  componentDidMount() {
    // // // // // //
    // VARIABLES
    var reposLimit;

    var orgReposUrlPromises = [];
    var allOrgReposData = [];

    var reposContributorsUrlPromises = [];

    var allContributorsPromises = [];
    var allContributorsData = [];

    var filteredContributorsData = [];
    var finalData = [];
    // // // // // //

    var orgReposPromise = getHeaders('https://api.github.com/orgs/angular/repos' + constants.TOKENFIRST);
    orgReposPromise.then(function(reposPageNumber) {
      for (var i=1; i <= reposPageNumber; i++) {
        let orgReposUrl = 'https://api.github.com/orgs/angular/repos?page=' + i + constants.TOKENLAST;
        orgReposUrlPromises.push(getData(orgReposUrl));
      }
      return Promise.all(orgReposUrlPromises)
    })
    .then(function(orgRepoData) {
      allOrgReposData = [].concat.apply([], orgRepoData);

      if ((constants.REPOSLIMIT > 0) && (constants.REPOSLIMIT <= allOrgReposData.length)) {
        reposLimit = constants.REPOSLIMIT;
      } else if (constants.REPOSLIMIT > allOrgReposData.length) {
        reposLimit = allOrgReposData.length;
      } else {
         reposLimit = 1;
      }

      for (var j=0; j < reposLimit; j++) {
        let repoContributorsUrl = allOrgReposData[j].contributors_url;
        reposContributorsUrlPromises.push(
          getHeaders(repoContributorsUrl + constants.TOKENFIRST)
          .then(function(result) {
            return [repoContributorsUrl + constants.TOKENFIRST, result]
          })
        )
      }
      return Promise.all(reposContributorsUrlPromises)
    })
    .then(function(repoContributorsPagesNumber) {
      for (var k=0; k < repoContributorsPagesNumber.length; k++) {
        if (repoContributorsPagesNumber[k][1] > 1) {
          for (var l=1; l <= repoContributorsPagesNumber[k][1]; l++) {
            let contributorsUrl = repoContributorsPagesNumber[k][0]+'&page='+l;
            allContributorsPromises.push(getData(contributorsUrl));
          }
        } else {
          allContributorsPromises.push(getData(repoContributorsPagesNumber[k][0]));
        }
      }
      return Promise.all(allContributorsPromises)
    })
    .then(function(contributorData) {
      allContributorsData = [].concat.apply([], contributorData);

      allContributorsData.forEach((item, index) => {
        const temp = filteredContributorsData.filter(e=>{return e.login === item.login});
        if (temp.length > 0) {
          filteredContributorsData[filteredContributorsData.findIndex(e=>{return e.login ===item.login})].contributions += item.contributions;
        }
        else {
          filteredContributorsData.push(item);
        }

        finalData = filteredContributorsData.map(function(contr) {
          return {
            id: contr["id"],
            login: contr["login"],
            avatar_url: contr["avatar_url"],
            contributions: contr["contributions"],
          };
        });

        this.setState({
          contributorsData: finalData,
          isLoadingRanking: 'loaded'
        });
      });
    }.bind(this))
    .catch(function(error){
      this.setState({
        isLoadingRanking: 'error',
        errorText: error
      });
    }.bind(this));
}

  loadStats() {
    // // // // // //
    // VARIABLES
    var contributorsStateData = this.state.contributorsData;
    var statsAllContributorsPromises = [];
    var statsAllContributorsData = [];
    var statsData = [];
    var mergedContributorsData = [];
    // // // // // //

    this.setState({
      isLoadingStats: 'loading'
    });

    for (var i=0; i<contributorsStateData.length; i++) {
      let contributorLogin = contributorsStateData[i].login;
      let contributorUrl = 'https://api.github.com/users/' + contributorLogin + constants.TOKENFIRST;
      statsAllContributorsPromises.push(getData(contributorUrl));
    }
    Promise.all(statsAllContributorsPromises)
    .then(function(contributorsData) {
      statsAllContributorsData = [].concat.apply([], contributorsData);

      statsData = statsAllContributorsData.map(function(stat) {
        return {
          id: stat["id"],
          public_repos: stat["public_repos"],
          public_gists: stat["public_gists"],
          followers: stat["followers"],
        };
      });

      mergedContributorsData = statsData.map(x => Object.assign(x, contributorsStateData.find(y => y.id === x.id)));

      this.setState({
        contributorsData: mergedContributorsData,
        isLoadingStats: 'loaded'
      });

    }.bind(this))
    .catch(function(error){
      this.setState({
        isLoadingStats: 'error',
        errorText: error
      });
    }.bind(this));
  }

  tableSort(field, event) {
    var tmpTab = this.state.contributorsData.slice();
    var descending = this.state.sortby === field && !this.state.descending;

    tmpTab.sort(function(a, b) {
      return descending
      ? (a[field] < b[field] ? 1 : -1)
      : (a[field] > b[field] ? 1 : -1);
    });

    this.setState({
      contributorsData: tmpTab,
      sortby: field,
      descending: descending
    });
  }

  render() {
    return (
      <div className="app__content">
        <h2>Organisation repositories contributors list with stats.</h2>
        <Loader isLoading={this.state.isLoadingRanking} errorText={this.state.errorText}>
          <table className="rankinglist">
            <thead>
              <tr>
              {
                this.state.contributorsHeadres.map(header => {
                  let headerTitle = header.header_title;

                  if (this.state.sortby === header.header_key) {
                    headerTitle += this.state.descending ? ' \u2191' : ' \u2193';
                  }

                  return(
                    <th key={header.header_key} onClick={this.tableSort.bind(this, header.header_key)}>{headerTitle}</th>
                  );
                })
              }
              </tr>
              <tr>
                <td colSpan="5"><p className="info">To show other stats (followers, repos & gists) clik this button: <button type="button" className="loadbtn" onClick={this.loadStats.bind(this)}>load stats</button><Loader isLoading={this.state.isLoadingStats} errorText={this.state.errorText}>!</Loader></p></td>
              </tr>
            </thead>
            <tbody>
            {this.state.contributorsData.map(contributor => {
              return (
                <tr key={contributor.id}>
                <td><Link to={'/contributor/' + contributor.login}><img src={logo} className="avatar avatar--micro" alt="" />{contributor.login}</Link></td>
                <td>{contributor.contributions}</td>
                <td>{contributor.followers}</td>
                <td>{contributor.public_repos}</td>
                <td>{contributor.public_gists}</td>
                </tr>
              );
            })}
            </tbody>
          </table>
        </Loader>
      </div>
    );
  }
}

export default Contributors;
