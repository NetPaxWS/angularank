import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as constants from '../Common/Constants';
import {
  getHeaders,
  getData
} from '../Common/Functions';
import Loader from './Loader';
import logo from '../Images/logo.svg';

class Contributor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reposData: [],
      isLoadingRepos: 'loading',
      errorText: ''
    };
  }

  componentDidMount() {
    // // // // // //
    // VARIABLES
    var orgReposUrlPromises = [];
    var allOrgReposData = [];
    // // // // // //

    var orgReposPromise = getHeaders('https://api.github.com/orgs/angular/repos' + constants.TOKENFIRST);
    orgReposPromise.then(function(reposPageNumber) {
      for (var i=1; i <= reposPageNumber; i++) {
        let orgReposUrl = 'https://api.github.com/orgs/angular/repos?page=' + i + constants.TOKENLAST;
        orgReposUrlPromises.push(getData(orgReposUrl));
      }
      return Promise.all(orgReposUrlPromises)
    })
    .then(function(orgRepoData) {
      allOrgReposData = [].concat.apply([], orgRepoData);
      this.setState({
        reposData: allOrgReposData,
        isLoadingRepos: 'loaded'
      });
    }.bind(this))
    .catch(function(error){
      this.setState({
        isLoadingRepos: 'error',
        errorText: error
      });
    }.bind(this));
  }

  render() {
    return (
      <div className="app__content">
        <h2>All repostories in "angular" organisation</h2>
        <Loader isLoading={this.state.isLoadingRepos}>
          <table className="rankinglist">
            <tbody>
              {this.state.reposData.map(repo => {
                return (
                  <tr key={repo.id}>
                  <td><Link to={'/repo/' + repo.name}><img src={logo} className="avatar avatar--micro" alt="" />{repo.name}</Link></td>
                  <td>{repo.full_name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Loader>
      </div>
    )}
}

export default Contributor;
