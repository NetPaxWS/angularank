import React from 'react';

const NotFound = () => {
  return(
    <div className="app__content">
      <h2>Error 404</h2>
      <p>Page not exists!</p>
    </div>
  );
};

export default NotFound;
