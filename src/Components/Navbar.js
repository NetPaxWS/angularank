import React from 'react';
import {
  NavLink
} from 'react-router-dom';
import './Navbar.css';

const Navbar = () => {
    return(
      <div className="navbar">
        <ul className="navbar__row">
          <li className="navbar__list"><NavLink to="/repos/" className="navbar__link" activeClassName="menu__link--active"><i className="fa fa-github-square navbar__icon"></i>All repos</NavLink></li>
          <li className="navbar__list"><NavLink to="/contributors/" className="navbar__link" activeClassName="menu__link--active"><i className="fa fa-list navbar__icon"></i>User Ranking</NavLink></li>
          <li className="navbar__list"><NavLink to="/about/" className="navbar__link" activeClassName="menu__link--active"><i className="fa fa-user-circle navbar__icon"></i>About / Author</NavLink></li>
        </ul>
      </div>
    );
};

export default Navbar;
