import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as constants from '../Common/Constants';
import {
  getHeaders,
  getData
} from '../Common/Functions';
import Loader from './Loader';

class Repo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      repoData: [],
      contributorsData: [],
      isLoadingRepo: 'loading',
      isLoadingContributors: 'loading',
      errorText: ''
    };
  }

  componentDidMount() {    
    var repoName = this.props.match.params.repoName;

    var repoPromise = getData('https://api.github.com/repos/angular/' + repoName + constants.TOKENFIRST);
    repoPromise.then(function(repo) {
      this.setState({
        repoData: repo,
        isLoadingRepo: 'loaded'
      });
      var repoContributorsPagesNumberPromise = getHeaders('https://api.github.com/repos/angular/' + repoName + '/contributors' + constants.TOKENFIRST);
      return repoContributorsPagesNumberPromise
    }.bind(this))
    .then(function(repoContributorsPagesNumber) {
      var contributorsPromises = [];
      for (var i=1; i <= repoContributorsPagesNumber; i++) {
        let contributorsUrl = 'https://api.github.com/repos/angular/' + repoName + '/contributors?page=' + i + constants.TOKENLAST;
        contributorsPromises.push(getData(contributorsUrl));
      }
      return Promise.all(contributorsPromises)
    })
    .then(function(contributorsData) {
      var allContributorsData = [];
      allContributorsData = [].concat.apply([], contributorsData);
      this.setState({
        contributorsData: allContributorsData,
        isLoadingContributors: 'loaded'
      });
    }.bind(this))
    .catch(function(error){
      console.log(error);
      this.setState({
        isLoadingRepo: 'error',
        isLoadingContributors: 'error',
        errorText: error
      });
    }.bind(this));
  }

  render() {
    return (
      <div className="app__content">
        <h2>Repository details and its contributors.</h2>
        <Loader isLoading={this.state.isLoadingRepo} errorText={this.state.errorText}>
          <p>Repo ID: {this.state.repoData.id}</p>
          <p>Name: {this.state.repoData.name}</p>
          <p>Full name: {this.state.repoData.full_name}</p>
          <p>Description: {this.state.repoData.description}</p>
          <p>Subscribers: {this.state.repoData.subscribers_count}</p>
          <hr />
          <p>See it on GitHub.com: <a href={this.state.repoData.html_url} target="_blank">click!</a></p>
        </Loader>
        <hr />
        <h3>Contributors of {this.state.repoData.name} repository & their contributions to this repo!</h3>
        <Loader isLoading={this.state.isLoadingContributors} errorText={this.state.errorText}>
          <table className="rankinglist">
            <tbody>
              {this.state.contributorsData.map(contributor => {
                return (
                  <tr key={contributor.id}>
                  <td><Link to={'/contributor/' + contributor.login}><img src={contributor.avatar_url} className="avatar avatar--micro" alt="" />{contributor.login}</Link></td>
                  <td>{contributor.contributions} contributions</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Loader>
      </div>
    );
  }
}

export default Repo;
