import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logo from '../Images/logo.svg';
import './Header.css';

class Header extends Component {
  render() {
    if (this.props.page === 'main') {
      var ctaButton = <Link to={'/repos/'} className="header__cta">click to start</Link>
    }

    return (
      <header className={(this.props.page==='main' ? 'header header--main' : 'header header--sub')}>
        <div className="header__row">
        </div>
        <Link to={'/'}>
          <img src={logo} className="header__logo" alt="Angularank by Wojciech Stawarz / @NetPax" />
        </Link>
        <div className="header__content">
          <h1 className="header__title">Angularank</h1>
          <p className="header__slogan">
            by Wojciech Stawarz / @NetPax for the challenge.
          </p>
          {ctaButton}
        </div>
      </header>
    );
  }
}

export default Header;
