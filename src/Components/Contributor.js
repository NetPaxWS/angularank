import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import * as constants from '../Common/Constants';
import {
  getHeaders,
  getData
} from '../Common/Functions';
import Loader from './Loader';
import logo from '../Images/logo.svg';

class Contributor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contributorData: [],
      reposData: [],
      isLoadingContributor: 'loading',
      isLoadingRepos: 'loading',
      errorText: ''
    };
  }

  componentDidMount() {
    // // // // // //
    // VARIABLES    
    var contributorLogin = this.props.match.params.contributorLogin;

    var allContributorReposData = [];
    var allOrgReposData = [];
    // // // // // //

    var contributorPromise = getData('https://api.github.com/users/' + contributorLogin + constants.TOKENFIRST);
    contributorPromise.then(function(contributor) {
      this.setState({
        contributorData: contributor,
        isLoadingContributor: 'loaded',
      });
      var contributorsReposPagesNumberPromise = getHeaders('https://api.github.com/users/' + contributorLogin + '/repos' + constants.TOKENFIRST);
      return contributorsReposPagesNumberPromise
    }.bind(this))
    .then(function(contributorsReposPagesNumber) {
      var reposPromises = [];
      for (var i=1; i <= contributorsReposPagesNumber; i++) {
        let reposUrl = 'https://api.github.com/users/' + contributorLogin + '/repos?page=' + i + constants.TOKENLAST;
        reposPromises.push(getData(reposUrl));
      }
      return Promise.all(reposPromises)
    })
    .then(function(contributorReposData) {
      allContributorReposData = [].concat.apply([], contributorReposData);

      var orgReposPagesNumberPromise = getHeaders('https://api.github.com/orgs/angular/repos' + constants.TOKENFIRST);
      return orgReposPagesNumberPromise
    })
    .then(function(orgReposPagesNumber) {
      var orgReposPromises = [];
      for (var j=1; j <= orgReposPagesNumber; j++) {
        let orgReposUrl = 'https://api.github.com/orgs/angular/repos?page=' + j + constants.TOKENLAST;
        orgReposPromises.push(getData(orgReposUrl));
      }
      return Promise.all(orgReposPromises)
    })
    .then(function(orgReposData) {
      allOrgReposData = [].concat.apply([], orgReposData);

      allContributorReposData = allContributorReposData.filter(function(n) {
        for(var i=0; i < allOrgReposData.length; i++){
          if(n.name === allOrgReposData[i].name){
            return true;
          }
        }
        return false;
      });

      this.setState({
        reposData: allContributorReposData,
        isLoadingRepos: 'loaded'
      });

    }.bind(this))
    .catch(function(error){
      this.setState({
        isLoadingContributor: 'error',
        isLoadingRepos: 'error',
        errorText: error
      });
    }.bind(this));
  }

  render() {
    return (
      <div className="app__content">
        <h2>Contributor details and repositories he contributed.</h2>
        <Loader isLoading={this.state.isLoadingContributor} errorText={this.state.errorText}>
          <img src={this.state.contributorData.avatar_url} className="avatar avatar--large" alt="" />
          <h3>{this.state.contributorData.name}</h3>
          <p>Login: {this.state.contributorData.login}</p>
          <p>Company: {this.state.contributorData.company}</p>
          <p>Location: {this.state.contributorData.location}</p>
          <hr />
          <p>Followers: {this.state.contributorData.followers}</p>
          <p>Public repos: {this.state.contributorData.public_repos}</p>
          <p>Public gists: {this.state.contributorData.public_gists}</p>
          <hr />
          <p>See it on GitHub.com: <a href={this.state.contributorData.html_url} target="_blank">click!</a></p>
          <p>blog: <a href={this.state.contributorData.blog} target="_blank">{this.state.contributorData.blog}</a></p>
        </Loader>
        <hr />
        <h3>Other contributors repositiories.</h3>
        <Loader isLoading={this.state.isLoadingRepos}>
          <table className="rankinglist">
            <tbody>
              {this.state.reposData.map(repo => {
                return (
                  <tr key={repo.id}>
                  <td><Link to={'/repo/' + repo.name}><img src={logo} className="avatar avatar--micro" alt="" />{repo.name}</Link></td>
                  <td>{repo.full_name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Loader>
      </div>
    )}
}

export default Contributor;
