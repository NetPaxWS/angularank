import React from 'react';

const About = () => {
    return(
      <div className="app__content">
        <h2>Angularank App v0.1.0 by Wojciech Stawarz</h2>
        <p>Hello! My name is Wojtek Stawarz. I coded this app and I want to thank You for reviewing this page.</p>
      </div>
    );
};

export default About;
