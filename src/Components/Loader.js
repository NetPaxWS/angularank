import React from 'react';
import PropTypes from 'prop-types';

const Loader = (props) => {
    if (props.isLoading === 'loading') {
      return <span><i className="fa fa-spinner fa-pulse fa-fw"></i> Loading...</span>;
    } else if (props.isLoading === 'loaded') {
      return props.children;
    } else {
      return <span className="error"><i className="fa fa-bug"></i> Error! Data could not be loaded! Response message: {props.errorText}</span>;
    }
};

Loader.propTypes = {
  isLoading: PropTypes.string.isRequired
};

export default Loader;
