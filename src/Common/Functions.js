var parse = require('parse-link-header');

export function getPagesNumber(headers) {
  if (headers) {
    var parsedHeaders = parse(headers);
    var pagesNumber = parseInt(parsedHeaders.last.page, 10);
  } else {
    pagesNumber = 1;
  }
  return pagesNumber;
}

export function getHeaders(url) {
  return new Promise(function(resolve, reject) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.onload = function() {
      if (xhttp.status === 200) {
        resolve(getPagesNumber(xhttp.getResponseHeader("Link")));
      } else {
        reject(xhttp.statusText);
      }
    };
    xhttp.onerror = function() {
      reject(xhttp.statusText);
    };
    xhttp.send();
  });
}

export function getData(url) {
  return new Promise(function(resolve, reject) {
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);
    xhttp.onload = function() {
      if (xhttp.status === 200) {
        resolve(JSON.parse(xhttp.response));
      } else {
        reject(xhttp.statusText);
      }
    };
    xhttp.onerror = function() {
      reject(xhttp.statusText);
    };
    xhttp.send();
  });
}
